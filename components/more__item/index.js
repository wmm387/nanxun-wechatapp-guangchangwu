const util = require('../../utils/util.js')

Component({
  properties: {
    item: Object
  },
  data: {},
  methods: {
    // 获取formId
    formSubmit: function(e) {
      let formId = e.detail.formId
      let id = e.currentTarget.dataset.id
      this.gotoDetail(id)
    },

    gotoDetail: function(id) {
      wx.navigateTo({
        url: '/pages/detail/detail?id=' + id
      })
    }
  }
})