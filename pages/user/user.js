const app = getApp()
const util = require('../../utils/util.js')

Page({
  data: {
    nowtapHistory: true,
    list: [],
  },
  onLoad: function(options) {
    this.getHistoryList()
  },
  onShow: function() {
    var that = this
    if (that.data.nowtapHistory) {
      that.getHistoryList()
    } else {
      that.getCollectList()
    }
  },

  onShareAppMessage: function() {},

  // 切换 历史记录 - 我的收藏
  changeTap: function() {
    var that = this
    that.setData({
      nowtapHistory: !that.data.nowtapHistory
    })
    if (that.data.nowtapHistory) {
      that.getHistoryList()
    } else {
      that.getCollectList()
    }
  },

  // 获取播放历史列表
  getHistoryList: function() {
    var that = this
    var historyList = wx.getStorageSync('historyList')
    if (historyList) {
      historyList = util.beSet(historyList)
      wx.setStorageSync('historyList', historyList)
      that.setData({
        list: historyList
      })
    }
  },

  // 获取收藏列表
  getCollectList: function() {
    var that = this
    wx.request({
      url: app.globalData.url + 'accs/get_favor_list',
      method: 'POST',
      data: {
        user_id: app.globalData.userId,
        collect_type: 'gcw'
      },
      success: function(res) {
        var list = res.data.data
        if (list) {
          that.setData({
            list: list
          })
        }
      }
    })
  },

  // 跳转广场舞详情
  gotoDetail: function (e) {
    let id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/detail/detail?id=' + id
    })
  },
})