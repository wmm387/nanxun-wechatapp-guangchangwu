const app = getApp()
const util = require('../../utils/util.js')

Page({
  data: {
    nowCateName: '广场舞精选',
    isCateShow: false,
    categoryList: [],
    itemList: [],
    page: 1,
    imageUrl: app.globalData.imageUrl,
    flags: {}
  },

  onLoad: function() {
    var that = this
    this.setData({
      page: 1
    })
    wx.showLoading({
      title: '加载数据中...',
      success: function() {
        that.getConfig()
        that.getRamdomList()
        that.getCategoryList()
      }
    })
  },

  // config
  getConfig: function() {
    var that = this
    wx.request({
      url: app.globalData.url + 'gcws/config/v/' + app.globalData.version,
      success: function(res) {
        if (res.data.data) {
          that.setData({
            flags: res.data.data
          })
        }
      }
    })
  },

  // 换分类
  changeCate: function() {
    var that = this

    that.setData({
      isCateShow: !that.data.isCateShow
    })
  },
  getCateVideoList: function(e) {
    var that = this

    var id = e.currentTarget.dataset.id
    var name = e.currentTarget.dataset.name
    // console.log(id);
    that.getCategoryInfo(id)
    that.setData({
      isCateShow: false,
      nowCateName: name
    })
  },

  // 获取广场舞分类
  getCategoryList: function() {
    var that = this
    wx.request({
      url: app.globalData.url + 'gcws/get_category/v/' + app.globalData.version,
      success: (res) => {
        that.setData({
          categoryList: res.data.data
        })
      }
    })
  },
  // 根据分类id获取对应分类下的视频
  getCategoryInfo: function(id) {
    var that = this
    wx.request({
      url: app.globalData.url + 'gcws/get_gcw_list/v/' + app.globalData.version + '/id/' + id + '/page/1',
      success: (res) => {
        that.setData({
          itemList: res.data.data,
          page: 1
        })
      }
    })
  },

  // 分享方法 
  onShareAppMessage: function() {},

  // 下拉刷新
  onPullDownRefresh: function() {
    wx.showNavigationBarLoading()
    this.onLoad()
  },

  // 上拉加载
  onReachBottom: function() {
    wx.showLoading({
      title: '加载数据中...',
      success: this.getOtherList(),
    })
  },

  // 获取随机广场舞数据
  getRamdomList: function() {
    var that = this
    wx.request({
      url: app.globalData.url + 'gcws/get_random_list/v/' + app.globalData.version,
      success: function(res) {
        if (res.data.data) {
          that.setData({
            itemList: res.data.data
          })

          wx.hideLoading()
          wx.hideNavigationBarLoading()
          wx.stopPullDownRefresh()
          wx.showToast({
            title: '已是最新',
            icon: 'success',
            duration: 1000
          })
        }
      },
      fail: function(e) {
        console.log(e)
      }
    })
  },

  // 获取广场舞列表
  getOtherList: function() {
    var that = this
    var page = this.data.page
    var itemList = this.data.itemList
    wx.request({
      url: app.globalData.url + 'gcws/get_normal_list/v/' + app.globalData.version + '/page/' + page,
      success: function(res) {
        var list = res.data.data
        if (list && list.length != 0) {
          that.setData({
            page: page + 1,
            itemList: itemList.concat(list),
          })
          wx.hideLoading()
        } else {
          wx.hideLoading()
          wx.showToast({
            title: '已经到底了',
            icon: 'success',
            duration: 1000
          })
        }
      },
      fail: function(e) {
        console.log(e)
      }
    })
  },

  // 换一批逻辑
  handleRamdom: function() {
    this.onLoad()
    wx.pageScrollTo({
      scrollTop: 0,
    })
  },

  // 获取formId
  formSubmit: function(e) {
    let formId = e.detail.formId
    let id = e.currentTarget.dataset.id
    this.gotoDetail(id)
  },

  // 跳转广场舞详情
  gotoDetail: function(id) {
    wx.navigateTo({
      url: '/pages/detail/detail?id=' + id
    })
  },

})