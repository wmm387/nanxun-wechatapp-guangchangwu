const app = getApp()

Page({

  data: {
    gcw: null,
    id: null,
    gcwList: [],
    navigatorList: [],
    isCollect: false,
    collectList: [],
    page: 1,
    imageUrl: app.globalData.imageUrl,
    flags: {},
    adTimer: false,
  },

  onLoad: function(options) {
    var _id = null
    if (options && options.id) {
      _id = options.id
    } else if (app.globalData.referrerInfo.extraData) {
      _id = app.globalData.referrerInfo.extraData['id']
    }
    var that = this
    this.getConfig()
    this.setData({
      page: 1,
      id: _id ? _id : this.data.id
    })
    wx.showLoading({
      title: '加载数据中...',
      success: this.getGcw()
    })
    this.getNavigatorList()
    this.getGcwList()
    this.timeer()
  },

  // config
  getConfig: function() {
    var that = this
    wx.request({
      url: app.globalData.url + 'gcws/config/v/' + app.globalData.version,
      success: function(res) {
        if (res.data.data) {
          that.setData({
            flags: res.data.data
          })
        }
      }
    })
  },

  // 下拉刷新
  onPullDownRefresh: function() {
    wx.showNavigationBarLoading()
    this.onLoad()
  },

  // 上拉加载
  onReachBottom: function() {
    wx.showLoading({
      title: '加载数据中...',
      success: this.getGcwList(),
    })
  },

  // 分享
  onShareAppMessage: function() {
    var gcw = this.data.gcw
    return {
      title: gcw.title,
    }
  },

  // 获取广场舞连接
  getGcw: function() {
    var that = this
    wx.request({
      url: app.globalData.url + 'gcws/get_gcw/v/' + app.globalData.version + '/id/' + this.data.id,
      success: function(res) {
        console.log(res)
        var gcw = res.data.data
        that.getIsCollect(gcw)
        that.handleHistory(gcw)
        that.setData({
          gcw: gcw,
        })
        wx.hideLoading()
        wx.hideNavigationBarLoading()
        wx.stopPullDownRefresh()
      },
      fail: function(e) {
        console.log(e)
      }
    })
  },

  // 获取跳转其它小程序列表
  getNavigatorList: function() {
    var that = this
    var navigatorList = this.data.navigatorList
    wx.request({
      url: app.globalData.url + 'gcws/get_navigator_list',
      success: function(res) {
        that.setData({
          navigatorList: res.data.data
        })
      },
      fail: function(error) {
        console.log(error)
      }
    })
  },

  // 获取广场舞列表
  getGcwList: function() {
    var that = this
    var page = this.data.page
    var gcwList = this.data.gcwList
    wx.request({
      url: app.globalData.url + 'gcws/get_normal_list/v/' + app.globalData.version + '/page/' + page,
      success: function(res) {
        var list = res.data.data
        if (list) {
          that.setData({
            gcwList: page == 1 ? list : gcwList.concat(list),
            page: page + 1,
          })
        }
        wx.hideLoading()
      },
      fail: function(error) {
        console.log(error)
      }
    })
  },

  // 处理收藏逻辑
  collect: function() {
    var that = this
    var gcw = this.data.gcw
    if (this.data.isCollect) {
      this.collectRequest(gcw.id, 'del')
    } else {
      this.collectRequest(gcw.id, 'add')
    }
  },

  // 判断是否收藏
  getIsCollect: function(gcw) {
    this.collectRequest(gcw.id, 'fetch')
  },

  // 收藏相关网络操作
  collectRequest: function(id, cType) {
    var that = this
    var url = app.globalData.url + 'accs/'
    var isCollect = true
    switch (cType) {
      case 'add':
        url = url + 'add_favor'
        break
      case 'del':
        url = url + 'del_favor'
        isCollect = false
        break
      case 'fetch':
        url = url + 'fetch_favor'
        break
    }
    wx.request({
      url: url,
      method: 'POST',
      data: {
        user_id: app.globalData.userId,
        favor_id: id,
      },
      success: function(res) {
        if (res.data.msg == 'ok') {
          that.setData({
            isCollect: isCollect
          })
        }
      }
    })
  },

  // 处理播放历史逻辑
  handleHistory: function(gcw) {
    var historyList = wx.getStorageSync('historyList') ? wx.getStorageSync('historyList') : []
    if (historyList.length > 30) {
      historyList.pop()
    }
    historyList.unshift(gcw)
    wx.setStorageSync('historyList', historyList)
  },

  // 定时器
  timeer: function() {
    var that = this
    setTimeout(function() {
      that.setData({
        adTimer: !that.data.adTimer        
      })
      that.timeer()
    }, 20000)
  }

})