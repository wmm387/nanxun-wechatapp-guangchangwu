App({
  onLaunch: function (options) {
    // 登录
    var that = this
    wx.login({
      success: res => {
        if (res.code) {
          wx.request({ 
            url: that.globalData.url + 'accs/login',
            method: 'POST',
            data: {
              code: res.code
            },
            success: function (res) {
              that.globalData.openId = res.data.data.user_id
              that.globalData.userId = res.data.data.openid
            }
          })
        } else {
          console.log('登录失败' + res.errMsg)
        }
      }
    })
  },
  onShow: function (options) {
    this.globalData.referrerInfo = options.referrerInfo
  },
  globalData: {
    version: '1.5.2',
    referrerInfo: null,
    userId: 0,
    openId: null,
    url: 'https://xcx.nanxuncn.com/api/gcw/',
    imageUrl: 'https://xcx.nanxuncn.com'
  }
})