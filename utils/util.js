// 根据id去重
const beSet = (arr) => {
  arr = arr.reverse()
  var result = [], i, j, len = arr.length;
  for (i = 0; i < len; i++) {
    for (j = i + 1; j < len; j++) {
      if (arr[i].id === arr[j].id) {
        j = ++i;
      }
    }
    result.push(arr[i]);
  }
  return result.reverse();
}

module.exports = {
  beSet: beSet
}